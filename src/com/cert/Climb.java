package com.cert;

public interface Climb {
    boolean isTooHigh(int height, int limit);
}
