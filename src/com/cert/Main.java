package com.cert;

interface MyString {
    String myStringFunction(String str);
}

class ReverseStr implements MyString {
    public String myStringFunction(String str) {
        String result = "";

        for(int i = str.length()-1; i >= 0; i--)
            result += str.charAt(i);

        return result;
    }
}

public class Main {

    public static void main(String[] args) {
        // write your code here
//        procedura();
        Absttract obj = new Extnd();
        System.out.println(obj.age(7));
        System.out.println(obj.num(8));
//        //Climber.clim();
//
//        //Persoana p = new Persoana("pop");
//       Student s = new Student("fam", "fmi");
//        Persoana p = s;
//        //((Student) p).printFacultate();
//        p.print();
    }

    public static void procedura() {
        String s = "Hello";
        String s2 = "Hello";
        String t = new String(s);

        System.out.println(System.identityHashCode(s));
        System.out.println(System.identityHashCode(s2));
        System.out.println(System.identityHashCode(t));

        if("Hello".equals(s))
            System.out.println("one");
        if(s2 == s)
            System.out.println("two");
        if(t.equals(s))
            System.out.println("three");
        if("Hello" == s)
            System.out.println("four");
        if("Hello" == t)
            System.out.println("five");
    }
}
