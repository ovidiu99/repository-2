package com.cert;

public interface HasTail {
    public boolean isTailStripped();
}
