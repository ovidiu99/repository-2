package com.cert;

public class Climber {
    public static void clim() {
        check(     (h,l) -> {return h > l;}, 11);

        Climb obj = new ClimbImpl();
        check(obj, 11);
    }
    private static void check(Climb climb, int height) {
        if(climb.isTooHigh(height,10))
            System.out.println("too high");
        else
            System.out.println("ok");
    }
}
