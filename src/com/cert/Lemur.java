package com.cert;

public class Lemur extends Primate implements HasTail {
    public boolean isTailStripped() {
        return false;
    }
    public int age = 10;
    public static void main(String[] args) {
        Lemur lemur = new Lemur();
        System.out.println(lemur.age);

        HasTail hasTail = lemur;
        System.out.println(((Lemur)hasTail).age);

        Primate primate = lemur;
        System.out.println(primate.hasHair());
    }
}
