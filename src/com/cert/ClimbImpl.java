package com.cert;

public class ClimbImpl implements Climb{
    @Override
    public boolean isTooHigh(int height, int limit) {
        return height > limit;
    }
}
