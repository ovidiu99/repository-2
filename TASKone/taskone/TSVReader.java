package taskone;

import java.io.File;
import java.io.IOException;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;

public class TSVReader {

    private String dirPath;
    private String line;
    private String key;
    private String values;
    private BufferedReader bufferedReader;
    private BufferedWriter bufferedWriter;


    public void setDirPath(String args) {
        Path currentRelativePath = Paths.get("");
        String currentPath = currentRelativePath.toAbsolutePath().toString();
        dirPath = currentPath + "\\" + args;
    }

    public void createWritingFileAndBufferedWriter() throws IOException {
        File newFile = new File(dirPath + "_agg.tsv");  //open the directory where the data will be written
        FileWriter fw = new FileWriter(newFile);
        bufferedWriter = new BufferedWriter(fw);
    }

    public void createReadingFileAndBufferedReader() throws FileNotFoundException {
        File file = new File(dirPath + ".tsv" );
        FileReader fr = new FileReader(file);
        bufferedReader = new BufferedReader(fr);
    }

    public Boolean isLine(BufferedReader br) throws IOException {
        if((line = br.readLine()) != null)
            return true;
        else
            return false;
    }

    public void setRecordsTSV(HashMap map) throws IOException{
        while(isLine(bufferedReader)) {
            RecordsTSV recordsTSV = new RecordsTSV();
            recordsTSV.setData(line);
            if(!(recordsTSV.getLiceu().equals("NEREPARTIZAT"))){
                setMapKeyValues(map,recordsTSV);
            }
        }
    }

    public void setMapKeyValues(HashMap<String,String> map, RecordsTSV recordsTSV) {
        values = recordsTSV.getPozitieMedie();
        key = recordsTSV.getLiceuProfilProfilDetLimba();
        map.put(key,values);
    }

    public void printMap(HashMap<String,String> map) throws IOException{
        map.forEach((k, v) ->  { try {bufferedWriter.write(k + '\t' + v); bufferedWriter.newLine(); } catch (IOException e) {} });
        bufferedWriter.close();
    }

    public static void main(String[] args) throws Exception{
        TSVReader tsvReader = new TSVReader();
        tsvReader.setDirPath(args[1]);
        HashMap<String,String> map = new HashMap<>();
        tsvReader.createWritingFileAndBufferedWriter();
        tsvReader.createReadingFileAndBufferedReader();
        tsvReader.setRecordsTSV(map);
        tsvReader.printMap(map);
    }
}



