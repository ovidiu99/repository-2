package taskone;

import java.io.File;
import java.io.IOException;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.BufferedWriter;
import java.nio.file.Path;
import java.nio.file.Paths;

public class SWFile {

    private String dirPath;
    private int pageCount = 1;
    private String line;
    private BufferedWriter bufferedWriter;


    public void setDirPath(String args) {
        Path currentRelativePath = Paths.get("");
        String currentPath = currentRelativePath.toAbsolutePath().toString();
        dirPath = currentPath + "\\" + args;
    }

    public String skipReadingLines(BufferedReader bufferedReader, int howManyTimes) throws IOException {
        for(int j = 0; j<howManyTimes; j++)
            line = bufferedReader.readLine();
        return line;
    }

    public void writeDataInFile(BufferedReader bufferedReader, String line) throws IOException {

        Records records = new Records();

        bufferedWriter.write(records.getId(line) + '\t');
        line = skipReadingLines(bufferedReader,1);
        bufferedWriter.write(records.getNume(line) + '\t');
        line = skipReadingLines(bufferedReader,1);
        bufferedWriter.write(records.getOras(line) + '\t');
        line = skipReadingLines(bufferedReader,1);
        bufferedWriter.write(records.getScoalaProv(line) + '\t');
        line = skipReadingLines(bufferedReader,1);
        bufferedWriter.write(records.getMediaAdm(line) + '\t');
        line = skipReadingLines(bufferedReader,1);
        bufferedWriter.write(records.getMediaEn(line) + '\t');
        line = skipReadingLines(bufferedReader,2);
        bufferedWriter.write(records.getMediaAbs(line) + '\t');
        line = skipReadingLines(bufferedReader,7);

        if(line.contains("<td class=tdc nowrap><img src=")) {
            bufferedWriter.write("NEREPARTIZAT");
            bufferedWriter.newLine();

        } else {
            bufferedWriter.write(records.getLiceuProfil(line) + '\t');
            line = skipReadingLines(bufferedReader,1);
            bufferedWriter.write(records.getProfDetSiLimba(line));
            bufferedWriter.newLine();
        }
    }

    public void createWritingFileAndBufferedWriter() throws IOException {
        File newFile = new File(dirPath + ".tsv");
        FileWriter fw = new FileWriter(newFile);
        bufferedWriter = new BufferedWriter(fw);

    }

    public BufferedReader createBufferedReader(File file) throws IOException {
        FileReader fr = new FileReader(file);                           //read data from the HTML files.
        return new BufferedReader(fr);
    }

    public void startThreads(Thread[] threads, int threadsNumber) {
        for (int i = 0; i < threadsNumber; i++)
            try{
                threads[i].start();
                threads[i].join();
            }catch(InterruptedException ine) {
                System.out.println(ine);
            }
    }

    public void closeBufferedWriter() {
        try {
            bufferedWriter.close();
        } catch (IOException ie) {
            System.out.println(ie);
        }
    }

    public void setAndStartThreads(int threadsNumber) {
        Thread[] threads = new Thread[threadsNumber];
        int[] pages = new int[threadsNumber + 1];
        BufferedReader[] bufferedReaders = new BufferedReader[threadsNumber + 1];
        File[] files = new File[threadsNumber+1];
        String[] lines = new String[threadsNumber+1];
        for(int i = 0; i < threadsNumber; i++) {
            int finalI = i + 1;
            threads[i] = new Thread(() -> {
               int thread = finalI;
               pages[thread] = thread;
               files[thread] = new File(dirPath + "\\page_" + pages[thread] + ".html");
               try {
                   while(files[thread].exists()) {
                       bufferedReaders[thread] = createBufferedReader(files[thread]);
                       while((lines[thread] = bufferedReaders[thread].readLine()) != null) {
                           if(lines[thread].contains("<td class=td>")) {
                               writeDataInFile(bufferedReaders[thread],lines[thread]);
                           }
                       }
                       pages[thread] = pages[thread] + threadsNumber;
                       files[thread] = new File(dirPath + "\\page_" + pages[thread] + ".html");
                   }
               } catch(IOException ie) {
                   System.out.println(ie);
               } catch(Exception e) {
                   System.out.println(e);
               }
            });
        }
        startThreads(threads,threadsNumber);
        closeBufferedWriter();
    }

    public static void main(String[] args) throws Exception {
        int threadsNumber = Integer.parseInt(args[1]);

        SWFile swfile = new SWFile();
        swfile.setDirPath(args[2]);
        swfile.createWritingFileAndBufferedWriter();
        swfile.setAndStartThreads(threadsNumber);


    }

}


