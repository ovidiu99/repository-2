package taskone;

public class RecordsTSV {
    private String pozitie = "";
    private String medie = "";
    private String liceu = "";
    private String pr = "";
    private String prDet = "";
    private String limb = "";
    private int i = 0;

    public void skipUntilTab(String line, int howManyTimes) {
        for(int j = 0; j < howManyTimes;j++) {
            while (line.charAt(i) != '\t') {
                i++;
            }
            i++;
        }
    }

    public String rememberCharactersUntilTab(String line) {
        String result = "";
        while(line.charAt(i) != '\t'){
            result = result + line.charAt(i);
            i++;
        }
        i++;
        return result;
    }

    public String rememberCharactersUntilEndOfLine(String line) {
        String result = "";
        while(i <= line.length()-1){
            result = result + line.charAt(i);
            i++;
        }
        return result;
    }

    public void setData(String line) {
        setPozitie(line);
    }

    public void setPozitie(String line){
        pozitie = rememberCharactersUntilTab(line);
        setMedie(line);
    }

    public void setMedie(String line) {

        skipUntilTab(line,3);
        if(!Character.isDigit(line.charAt(i)))
            i++;
        medie = rememberCharactersUntilTab(line);
        setLiceu(line);

    }

    public void setLiceu(String line) {
        skipUntilTab(line,2);
        if(!line.substring(i).equals("NEREPARTIZAT")) {
            liceu = rememberCharactersUntilTab(line);
            setProfil(line);
        } else {
            liceu = liceu + "NEREPARTIZAT";
        }
    }

    public void setProfil(String line) {
        pr = rememberCharactersUntilTab(line);
        setProfilDetaliat(line);
    }

    public void setProfilDetaliat(String line) {
        prDet = rememberCharactersUntilTab(line);
        setLimba(line);
    }

    public void setLimba(String line) {
        limb = rememberCharactersUntilEndOfLine(line);
    }

    public String getPozitieMedie() {
        return pozitie + '\t' + medie;
    }

    public String getLiceuProfilProfilDetLimba() {
        return liceu + '\t' + pr + '\t' + prDet + '\t' + limb;
    }

    public String getLiceu() {
        return liceu;
    }

}
