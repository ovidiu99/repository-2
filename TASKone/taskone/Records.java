package taskone;

public class Records {
    private String id = "";
    private String nume = "";
    private String oras = "";
    private String scoalaProv = "";
    private String medieAdm = "";
    private String medieEn = "";
    private String medieAbs = "";
    private String liceu = "";
    private String profil = "";
    private String profilDet = "";
    private String limba = "";

    public int skipCharacters(String line, int i, int howManyTimes) {
        for(int j = 0; j < howManyTimes; j++){ //how many times to skip until '>';
            while(line.charAt(i) != '>')
                i++;
            i++;
        }
        return i;
    }

    public String rememberCharacters(String line, int i) {
        String result = "";
        while(line.charAt(i) != '<') {
            result = result + line.charAt(i);
            i++;
        }
        return result;
    }

    public String getId(String line) {
        int i = skipCharacters(line,0,1);
        id = rememberCharacters(line,i);
        return id;
    }

    public String getNume(String line) {
        int i = skipCharacters(line,0,1);
        nume = rememberCharacters(line,i);
        return nume;
    }

    public String getOras(String line) {
        int i = skipCharacters(line,0,2);
        oras = rememberCharacters(line,i);
        return oras;
    }

    public String getScoalaProv(String line) {
        int i = skipCharacters(line,0,2);
        scoalaProv = rememberCharacters(line,i);
        return scoalaProv;
    }

    public String getMediaAdm(String line) {
        int i = skipCharacters(line,0,1);
        medieAdm = rememberCharacters(line,i);
        return medieAdm;
    }

    public String getMediaEn(String line) {
        int i = skipCharacters(line,0,1);
        medieEn = rememberCharacters(line,i);
        return medieEn;
    }

    public String getMediaAbs(String line) {
        int i = skipCharacters(line,0,1);
        medieAbs = rememberCharacters(line,i);
        return medieAbs;
    }

    public String getLiceuProfil(String line) {
        //Liceu
        int i = skipCharacters(line,0,2);
        liceu = rememberCharacters(line,i);

        //PROFIL
        i = skipCharacters(line,i,3);
        profil = rememberCharacters(line,i);

        return liceu + '\t' + profil;
    }

    public String getProfDetSiLimba(String line) {
        //PROFIL DETALIAT
        int i = skipCharacters(line,0,2);
        profilDet = rememberCharacters(line,i);

        //LIMBA
        i = skipCharacters(line,i,3);
        limba = rememberCharacters(line,i);

        return profilDet + '\t' + limba;
    }

}
