package taskone;

import java.io.File;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.FileWriter;
import java.io.BufferedWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Path;
import java.nio.file.Paths;

public class URLReader {

    private String dirPath;
    private String urlNamePrincipal;
    private URL url;

    public File createDirectory(String directory) {
        File theDir = new File(directory);
        return theDir;
    }

    public void setDirPath(String args) {
        Path currentRelativePath = Paths.get("");
        String currentPath = currentRelativePath.toAbsolutePath().toString();
        dirPath = currentPath + "\\" + args;
    }

    public int urlStatus(String urlString) throws IOException {

        URL u = new URL(urlString);
        HttpURLConnection huc = (HttpURLConnection) u.openConnection();
        huc.setRequestMethod("GET");
        huc.connect();
        return huc.getResponseCode();
    }


    public BufferedWriter createWritingFileAndBufferedWriter(int page) throws IOException {
        String writingPath = dirPath + "\\" + "page_" + page + ".html";
        File newFile = new File(writingPath);
        FileWriter fw = new FileWriter(newFile);
        return new BufferedWriter(fw);
    }

    public void setUrlNamePrincipal(String year) {
        urlNamePrincipal = "http://static.admitere.edu.ro/" + year;
    }

    public URLConnection getUrlConn(String urlName) throws Exception {
        url = new URL(urlName);
        URLConnection conn = url.openConnection();
        return conn;
    }

    public void getDataAndWriteInFile(BufferedWriter bufferedWriter, URLConnection conn) throws IOException {
        String inputLine;
        BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        while ((inputLine = br.readLine()) != null) {
            bufferedWriter.write(inputLine);
            bufferedWriter.newLine();
        }
        bufferedWriter.close();
        br.close();
    }

    public void startThreads(Thread[] threads, int threadsNumber) {
        for (int i = 0; i < threadsNumber; i++)
            threads[i].start();
    }

    public void setAndStartThreads(int threadsNumber) {
        Thread[] threads = new Thread[threadsNumber];
        int[] pages = new int[threadsNumber+1];
        BufferedWriter[] bufferedWriters = new BufferedWriter[threadsNumber+1];
        String[] urlNames = new String[threadsNumber+1];
        for (int i = 0; i < threadsNumber; i++) {
            int finalI = i + 1;
            threads[i] = new Thread(() -> {
                int thread = finalI;
                pages[thread] = thread;
                urlNames[thread] = urlNamePrincipal + "/staticRepI/j/B/cin/page_" + pages[thread] + ".html";
                try {
                    while (urlStatus(urlNames[thread]) == 200) {
                        bufferedWriters[thread] = createWritingFileAndBufferedWriter(pages[thread]);
                        URLConnection connection = getUrlConn(urlNames[thread]);
                        getDataAndWriteInFile(bufferedWriters[thread], connection);
                        pages[thread] = pages[thread] + threadsNumber;
                        urlNames[thread] = urlNamePrincipal + "/staticRepI/j/B/cin/page_" + pages[thread] + ".html";
                    }
                    bufferedWriters[thread].close();
                } catch (IOException ie) {
                    System.out.println(ie);
                } catch (Exception e) {
                    System.out.println(e);
                }
            });
        }
        startThreads(threads,threadsNumber);

    }


    public static void main(String[] args) {
        int threadsNumber = Integer.parseInt(args[1]);

        URLReader urlReader = new URLReader();

        urlReader.setDirPath(args[2]);
        urlReader.createDirectory(urlReader.dirPath).mkdir();
        urlReader.setUrlNamePrincipal(args[2]);

        urlReader.setAndStartThreads(threadsNumber);

    }
}

